class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :name
      t.string :url
      t.text :content
      t.string :ancestry
      t.string :menu_name
      t.integer :weight

      t.timestamps
    end
    add_index :pages, :name
    add_index :pages, :url
  end
end
