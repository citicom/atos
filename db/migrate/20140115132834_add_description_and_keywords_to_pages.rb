class AddDescriptionAndKeywordsToPages < ActiveRecord::Migration
  def change
    add_column :pages, :description, :text
    add_column :pages, :keywords, :text
  end
end
