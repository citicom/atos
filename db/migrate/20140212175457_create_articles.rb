class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :content
      t.text :description
      t.text :keywords
      t.string :name
      t.string :title
      t.string :url

      t.timestamps
    end
  end
end
