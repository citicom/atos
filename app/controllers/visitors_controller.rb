class VisitorsController < ApplicationController

  def page
    if %w(index Index uvod Uvod home Home).include?(params[:id])
      redirect_to action: :index
    else
      @page = Page.find_by_url(params['id'])
      if @page.nil?
        redirect_to(page_not_found_path)
      else
        path = @page.path.map{|p| "<a href='/#{p.url}'>#{p.menu_name}</a>"}
        @path = path.length > 1 ? path.join(' / ') : ''
        @title = @page.title ? @page.title : @page.name
      end
    end
  end

  def blog
    @article = Article.where(url: params['id']).first
    if @article.nil?
      @articles = Article.all
      @title = "Blog"
    else
      @title = @article.title ? @article.title : @article.name
    end
  end

  def index
    @slide_show = true
  end

  def site_map

  end

  def send_contact
    if verify_recaptcha
      ContactMailer.contact_mail(params["contactm"],params["email"]).deliver
      @message = "<div class='notifications success'><p><strong>Vaša správa bola odoslaná.</strong> Ďakujeme a prajeme pekný deň</p><span class='closer'><i class='icon-cancel-1'></i></span></div>"
    else
      @message = "<div class='notifications error'><p><strong>Nesprávne vyplnený formulár.</strong> Prosím skontrolujte  správnosť údajov (e-mail, správa, overovací kód)</p><span class='closer'><i class='icon-cancel-1'></i></span></div>"
    end
  end
end
