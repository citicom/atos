// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
// require turbolinks
//= require bootstrap
//= require_tree ./jslib
//= require grid
//= require superfish
//= require mypassion

var currentMenu;

currentMenu = function() {
    var a, current_url, li;
    a = $(location).attr('href').split('/');
    current_url = "/" + a[a.length - 1].split("#")[0];
    $("li.current").removeClass("current");
    li = $('.sf-menu a[href$="' + current_url + '"]').parent('li');
    if (li.parents('li').length !== 0) {
        return li.parents('li').addClass('current');
    } else {
        return li.addClass('current');
    }
};
//    var a, current_url, li;
//    a = $(location).attr('href').split('/');
//    current_url = "/" + a[a.length - 1].split("#")[0];
//    $("li.current").removeClass("current");
//    li = $('.sf-menu a[href^="' + current_url + '"]').parent('li');
//    if (li.parents('li').length !== 0) {
//        li.parents('li').addClass('current');
//    } else {
//        li.addClass('current');
//    };

$(document).load(currentMenu());
$(document).bind('page:load', currentMenu());

//$(document).ready(function() {
//    jQuery.validator.setDefaults({
//        errorPlacement: function(error, element) {
//            // if the input has a prepend or append element, put the validation msg after the parent div
//            if(element.parent().hasClass('input-prepend') || element.parent().hasClass('input-append')) {
//                error.insertAfter(element.parent());
//                // else just place the validation message immediatly after the input
//            } else {
//                error.insertAfter(element);
//            }
//        },
//        errorElement: "small", // contain the error msg in a small tag
//        wrapper: "div", // wrap the error message and small tag in a div
//        highlight: function(element) {
//            $(element).closest('.control-group').addClass('error'); // add the Bootstrap error class to the control group
//        },
//        success: function(element) {
//            $(element).closest('.control-group').removeClass('error'); // remove the Boostrap error class from the control group
//        }
//    });
//});

$(document).on('page:change', function() {
    if (window._gaq != null) {
        return _gaq.push(['_trackPageview']);
    } else if (window.pageTracker != null) {
        return pageTracker._trackPageview();
    }
});
