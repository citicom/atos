class ContactMailer < ActionMailer::Base
  default from: ENV['GMAIL_USERNAME']

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_mailer.ContactMail.subject
  #
  def contact_mail(message, email_from)
    @message = message
    @email_from = email_from
    mail(to: YAML.load(ENV['CONTACT_TO']), subject: 'Email z kontaktneho formulara stranky atos.sk')
  end
end
