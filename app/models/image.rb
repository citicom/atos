class Image < ActiveRecord::Base
  belongs_to :page, inverse_of: :images

  has_attached_file :img, styles: { thumb: "100x100", portfolio_thumb: "320x229#", portfolio: "1280x720"}, convert_options: {portfolio_thumb:  "-quality 75 -strip", portfolio:  "-quality 85 -strip"}

  def name
    alt
  end

  def delete_img
    @image_delete ||= "0"
  end

  def delete_img=(value)
    @image_delete = value
  end

  before_validation { self.img.clear if self.delete_img == '1' }
end
