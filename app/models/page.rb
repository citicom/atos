class Page < ActiveRecord::Base
  has_ancestry
  has_many :images, dependent: :destroy, inverse_of: :page
  accepts_nested_attributes_for :images, allow_destroy: true
  validates_uniqueness_of :url
  validates :url, uniqueness: true, exclusion: {in: %w( admin index users ckeditor 404 pages pictures attachment_files site-map uploader)}
  validates_presence_of :url, :name

  after_create :expire_cache
  after_save :expire_cache
  after_destroy :expire_cache

  private
  def expire_cache
    ActionController::Base.new.expire_fragment 'menu'
  end

end
