class Article < ActiveRecord::Base
  validates_uniqueness_of :url
  validates :url, uniqueness: true, exclusion: {in: %w( admin index users ckeditor 404 pages pictures attachment_files site-map uploader)}
  validates_presence_of :url, :name

  default_scope { order('created_at desc') }

  after_create :expire_cache
  after_save :expire_cache
  after_destroy :expire_cache

  private
  def expire_cache
    ActionController::Base.new.expire_fragment 'articles'
  end
end
