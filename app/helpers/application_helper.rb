module ApplicationHelper

  def url_gen(url)
    "#{request.protocol}#{request.host_with_port}#{url}"
  end
end
