Atos::Application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Ckeditor::Engine => '/ckeditor'
  root to: 'visitors#index'
  get '/site-map', to: 'visitors#site_map'
  get '/404', to: 'high_voltage/pages#show', id: '404', as: :page_not_found
  post '/contact', to: 'visitors#send_contact', as: :send_contact
  get '/kontakt', to: 'high_voltage/pages#show', id: 'contact', as: :contact
  get '/index', to: 'visitors#index', as: :index
  get '/blog/:id', to: 'visitors#blog', as: :article
  get '/blog', to: 'visitors#blog', as: :blog
  get '/:id', to: 'visitors#page'
end
