require "bundler/capistrano"
require "rvm/capistrano" # Load RVM's capistrano plugin.
require 'new_relic/recipes'

server "198.211.126.26", :web, :app, :db, primary: true

# Add RVM's lib directory to the load path.
#$:.unshift(File.expand_path('./lib', ENV['rvm_path']))

set :rvm_ruby_string, '2.0.0'
set :rvm_type, :system  # Don't use system-wide RVM

set :application, "atos"
set :user, "deployer"
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false
set :bundle_without, [:development, :test]
set :keep_releases, 5
set :scm, "git"
set :repository, "git@bitbucket.org:citicom/#{application}.git"
set :branch, "master"


default_run_options[:pty] = true
ssh_options[:forward_agent] = true

#before 'deploy:setup', 'rvm:install_rvm'
#before 'deploy:setup', 'rvm:install_ruby'

after "deploy", "deploy:cleanup" # keep only the last 5 releases


namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end
after "deploy", "rvm:trust_rvmrc"

namespace :deploy do
  #task :start do; end
  #task :stop do; end
  #task :restart, roles: :app, except: {no_release: true} do
  #  run "touch #{deploy_to}/current/tmp/restart.txt"
  #end
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "cd #{current_path} && /etc/init.d/unicorn_#{application} #{command}"
    end
  end
  after "deploy:update", "deploy:restart"

  task :link_public_folder, roles: [:app, :web] do
    run "mkdir -p #{shared_path}/public/fonts"
    run "mv -u #{release_path}/public/fonts/* #{shared_path}/public/fonts"
    #run "mv -u #{release_path}/public/* #{shared_path}/public"
    run "rm -rf #{release_path}/public"
    run "ln -s #{shared_path}/public #{release_path}/public"
  end
  after "deploy:update", "deploy:link_public_folder"
  after "deploy:update", "deploy:migrations"
  after "deploy:migrations", "deploy:db_seed"

  task :setup_config, roles: :app do
    #sudo "ln -nfs #{current_path}/config/apache.conf /etc/apache2/sites-available/#{application}"
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-available/#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
    run "mkdir -p #{shared_path}/config"
    run "mkdir -p #{shared_path}/public"
    run "mkdir -p #{shared_path}/public/ckeditor_assets"
    run "ln -s #{shared_path}/ckeditor_assets #{release_path}/public/ckeditor_assets"
    # put File.read("config/database.yml"), "#{shared_path}/config/database.yml"
    put File.read("config/application.example.yml"), "#{shared_path}/config/application.yml"
    #run "cp #{current_path}/config/application.example.yml #{shared_path}/config/application.yml"
    puts "Now edit the config files in #{shared_path}."
  end
  after "deploy:setup", "deploy:setup_config"
  task :db_seed, roles: :db do
    run "cd #{current_path};bundle exec rake db:migrate RAILS_ENV=production"
    run "cd #{current_path};bundle exec rake db:seed RAILS_ENV=production"
  end
  after "deploy:setup", "deploy:db_seed"
  after "deploy:update", "deploy:db_seed"

  task :symlink_config, roles: :app do
    # run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
    run "mkdir -p #{release_path}/tmp/sock"
  end
  after "deploy:finalize_update", "deploy:symlink_config"

  desc "Make sure local git is in sync with remote."
  task :check_revision, roles: :web do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Run `git push` to sync changes."
      exit
    end
  end
  before "deploy", "deploy:check_revision"

end
# We need to run this after our collector mongrels are up and running
# This goes out even if the deploy fails, sadly
after "deploy", "newrelic:notice_deployment"
after "deploy:update", "newrelic:notice_deployment"
after "deploy:migrations", "newrelic:notice_deployment"
after "deploy:cold", "newrelic:notice_deployment"

#paperclip
namespace :deploy do
  desc "build missing paperclip styles"
  task :build_missing_paperclip_styles, :roles => :app do
    run "cd #{release_path}; RAILS_ENV=production bundle exec rake paperclip:refresh:missing_styles"
  end
end

after "deploy:update_code", "deploy:build_missing_paperclip_styles"
