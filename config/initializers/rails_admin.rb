# RailsAdmin config file. Generated on August 15, 2013 15:18
# See github.com/sferik/rails_admin for more informations

RailsAdmin.config do |config|


  ################  Global configuration  ################

  # Set the admin name here (optional second array element will appear in red). For example:
  config.main_app_name = ['Atos', 'Admin']
  # or for a more dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }

  # RailsAdmin may need a way to know who the current user is]
  config.current_user_method { current_user } # auto-generated

  config.authenticate_with do
    warden.authenticate! :scope => :user
  end
  # If you want to track changes on your models:
  config.audit_with :history, 'Page'

  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, 'User'

  # Display empty fields in show views:
  # config.compact_show_view = false

  # Number of default rows per-page:
  # config.default_items_per_page = 20

  # Exclude specific models (keep the others):
  # config.excluded_models = []

  # Include specific models (exclude the others):
  config.included_models = %w( Page Image Article)

  # Label methods for model instances:
  # config.label_methods << :description # Default is [:name, :title]


  config.actions do
    # root actions
    dashboard                     # mandatory
                                  # collection actions
    index                         # mandatory
    new
    export
    history_index
    bulk_delete
                                  # member actions
    show
    edit
    delete
    history_show
    show_in_app

    # Add the nestable action for each model
    nestable do
      visible do
        %w[Page].include? bindings[:abstract_model].model_name
      end
    end
  end

  ################  Model configuration  ################

  # Each model configuration can alternatively:
  #   - stay here in a `config.model 'ModelName' do ... end` block
  #   - go in the model definition file in a `rails_admin do ... end` block

  # This is your choice to make:
  #   - This initializer is loaded once at startup (modifications will show up when restarting the application) but all RailsAdmin configuration would stay in one place.
  #   - Models are reloaded at each request in development mode (when modified), which may smooth your RailsAdmin development workflow.


  # Now you probably need to tour the wiki a bit: https://github.com/sferik/rails_admin/wiki
  # Anyway, here is how RailsAdmin saw your application's models when you ran the initializer:

  config.model Page do
    nestable_tree({
                      position_field: :weight,
                      max_depth: 3
                  })
    edit do
      field :title, :string
      field :name, :string
      field :menu_name, :string
      field :url, :string
      field :parent_id, :enum do
        enum do
          except = bindings[:object].id.nil? ? 0 : bindings[:object].id
          Page.where("id != ?", except).map { |p| [ p.name, p.id ] }
        end
      end
      field :content, :ck_editor
      field :description, :text
      field :keywords, :text
      field :images, :has_many_association do
        label I18n.translate('activerecord.attributes.page.gallery')
      end
    end
    show do
      field :name, :text do
        pretty_value do
          address =  bindings[:object].ancestors.map{|p| p.name}
          address = address << bindings[:object].name
          address.join(" / ")
        end
      end
      field :content, :text do
        pretty_value do
          bindings[:object].content.html_safe
        end
      end
    end
    list do
      field :name, :string
      field :ancestry, :integer do
        pretty_value do
          bindings[:object].parent_id.nil? ? "-" : bindings[:object].parent.name
        end
      end
      field :created_at
    end
  end

  config.model Image do
    edit do
      field :page, :belongs_to_association
      field :alt, :string
      field :img, :paperclip
    end
  end

  config.model Article do

    edit do
      field :title, :string
      field :name, :string
      field :url, :string
      field :content, :ck_editor
      field :description, :text
      field :keywords, :text
    end
    show do
      field :name, :text do
        pretty_value do
          address =  bindings[:object].ancestors.map{|p| p.name}
          address = address << bindings[:object].name
          address.join(" / ")
        end
      end
      field :content, :text do
        pretty_value do
          bindings[:object].content.html_safe
        end
      end
    end
    list do
      field :name, :string
      field :created_at
    end
  end

end
